# (c) 2017, Fredrik Rambris <fredrik@rambris.com>
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
from subprocess import check_output
try:
    import simplejson as json
except ImportError:
    import json

class VarsModule(object):

    """
    Loads variables for groups and/or hosts
    """

    def __init__(self, inventory):

        """ constructor """

        self.inventory = inventory
        self.inventory_basedir = inventory.basedir()


    def run(self, host, vault_password=None):
        """ For backwards compatibility, when only vars per host were retrieved
            This method should return both host specific vars as well as vars
            calculated from groups it is a member of """
        return {}


    def get_host_vars(self, host, vault_password=None):
        """ Get host specific variables. """
        return {}


    def get_group_vars(self, group, vault_password=None):
        """ Gets info about the current git repo and inserts it into the 'all' group_vars """

        if(group.name == 'all'):
            template = '{%n  "commit": "%H",%n  "abbreviated_commit": "%h",%n  "tree": "%T",%n  "abbreviated_tree": "%t",%n  "parent": "%P",%n  "abbreviated_parent": "%p",%n  "refs": "%D",%n  "encoding": "%e",%n  "subject": "%s",%n  "sanitized_subject_line": "%f",%n  "commit_notes": "%N",%n  "verification_flag": "%G?",%n  "signer": "%GS",%n  "signer_key": "%GK",%n  "author": {%n    "name": "%aN",%n    "email": "%aE",%n    "date": "%aD"%n  },%n  "commiter": {%n    "name": "%cN",%n    "email": "%cE",%n    "date": "%cD"%n  }%n}'
            git_log=check_output(["git", "log", "-n1", "--pretty=format:%s" % (template)])
            git_body=check_output(["git", "log", "-n1", "--pretty=format:%b"])
            git_branch=check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"])
            git = json.loads(git_log)
            git['branch'] = git_branch.split("\n")[0]
            git['body'] = git_body.replace("\r\n", "\n")
            return { 'git': git }
        else:
            return {}


# (c) 2017, Fredrik Rambris <fredrik@rambris.com>
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import sys

class VarsModule(object):

    """
    Loads variables for groups and/or hosts
    """

    def __init__(self, inventory):

        """ constructor """

        self.inventory = inventory
        self.inventory_basedir = inventory.basedir()


    def run(self, host, vault_password=None):
        """ For backwards compatibility, when only vars per host were retrieved
            This method should return both host specific vars as well as vars
            calculated from groups it is a member of """
        return {}


    def get_host_vars(self, host, vault_password=None):
        """ Get host specific variables. """
        return {}


    def get_group_vars(self, group, vault_password=None):
        """ Inserts current command line args into the 'all' group_vars """

        if(group.name == 'all'):
            return { "cmdline": sys.argv }
        else:
            return {}

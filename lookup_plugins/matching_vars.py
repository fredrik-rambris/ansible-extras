# (c) 2017, Fredrik Rambris <fredrik@rambris.com>
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.lookup import LookupBase
from ansible.errors import AnsibleLookupError
import re

"""
Lookup plugin to get a list of all variables whose name matches given
regular expressions. Does not recurse.

with_matching_vars: ".*_versions"
"""

class LookupModule(LookupBase):

    def run(self, terms, variables=None, **kwargs):

        if not isinstance(terms, list):
            terms=[terms]

        if 'vars' in kwargs:
            vars = kwargs['vars']
        else:
            vars = variables

        return_value = []

        for term in terms:
            try:
                pattern = re.compile(term)
            except Exception as e:
                raise AnsibleLookupError("matching_vars: %s" % e)

            for (name, value) in vars.iteritems():
                if pattern.match(name):
                    return_value.extend(self._templar.template(value))

            return return_value